import json
import re
import scrapy

URL = "https://shop.mango.com/bg-en/men/t-shirts-plain/100-linen-slim-fit-t-shirt_47095923.html"

REQUEST_URL = "https://shop.mango.com/services/garments/068/en/S/"

pattern = r"(\d+)\.html"
match = re.search(pattern, URL)

if match:
    product_id = match.group(1)


class ProductsSpider(scrapy.Spider):
    name = "api"

    def start_requests(self):
        yield scrapy.Request(REQUEST_URL + product_id, callback=self.parse)

    def parse(self, response):
        result = json.loads(response.text)

        product_name: str = result["name"]
        price = result["price"]["price"]
        colours: list = result["colors"]["colors"]
        colours_readable: list[str] = []
        size_availability: list[dict] = []

        for colour in colours:
            readible_colour = colour["label"]
            sizes_of_colour = colour["dataLayer"]["sizeAvailability"].split(",")

            # some of the API outputs are in spanish
            if sizes_of_colour == ["ninguno"]:
                sizes_of_colour = None

            colours_readable.append(readible_colour)
            size_availability.append({readible_colour: sizes_of_colour})

        return {
            "name": product_name,
            "price": price,
            "colour": colours_readable,
            "sizes": size_availability,
        }
