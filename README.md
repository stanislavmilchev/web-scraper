# Web scraper

Two variations have been tested. As the web content to be scraped is dynamic, basic html scraping cannot solve the issue. 
1. Using a headless browser to load the content and *then* parse it.
2. Directly extracting product ID and using the API to make a request and get back all of the product's data.


## Getting started

To run the spiders:
```
scrapy crawl headless
```
```
scrapy crawl api
```
To generate an output:

```
scrapy crawl headless -O output.json
```