import scrapy
from scrapy_playwright.page import PageMethod

URL = "https://shop.mango.com/bg-en/men/t-shirts-plain/100-linen-slim-fit-t-shirt_47095923.html"
URL = "https://shop.mango.com/bg-en/men/blazers-formal/stretch-fabric-slim-fit-suit-jacket_57040545.html"


class HeadlessSpider(scrapy.Spider):
    name = "headless"

    def start_requests(self):
        yield scrapy.Request(
            URL,
            meta={
                "playwright": True,
                "playwright_include_page": True,
                "playwright_page_methods": [
                    PageMethod(
                        "wait_for_selector",
                        "//*[@id='productDesktop']/main/div/div[2]/div[2]/div[2]/div/div/span[1]",
                    ),
                    PageMethod(
                        "wait_for_selector",
                        "//*[@id='sizesContainer']",
                    ),
                ],
            },
        )

    def parse(self, response):
        product_name = response.css("h1.product-name::text").get()
        selected_colour = response.css("span.colors-info-name::text").get()
        price = response.css("meta[itemprop='price']").attrib.get("content")

        available_sizes = []

        sizes_list = response.css("ul[aria-label='Select your size']")
        size_elements = sizes_list.css("li")

        for size_element in size_elements:
            size_select_button = size_element.css("button")
            size_span = size_select_button.css("span")[0]

            size_available = size_span.attrib.get("data-available")
            size_string = size_span.css("::text").get()

            if size_available:
                available_sizes.append(size_string)

        return {
            "name": product_name,
            "colour": selected_colour,
            "price": price,
            "available_sizes": available_sizes,
        }
